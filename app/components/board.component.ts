import { Component } from '@angular/core';
import { ChessSquareComponent } from './chess-square.component';

@Component({
    selector: 'board-standard',
    templateUrl: 'app/templates/board.component.html',
    directives: [ChessSquareComponent],
    styleUrls: ['app/styles/chess-square.component.css']
})

export class BoardComponent {
    public squares = [];

    constructor() {
        for (let i = 0; i < 64; i++) {
            if (
                (i < 8) ||
                (i > 15 && i < 24) ||
                (i > 31 && i < 40) ||
                (i > 47 && i < 56)
            ) {
                if (i % 2 == 0) {
                    this.squares.push({'type': 'light'});
                } else {
                    this.squares.push({'type': 'dark'});
                }
            } else {
                if (i % 2 == 0) {
                    this.squares.push({'type': 'dark'});
                } else {
                    this.squares.push({'type': 'light'});
                }
            }
        }
    }
}