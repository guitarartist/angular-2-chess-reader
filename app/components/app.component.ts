import { Component } from '@angular/core';
import {BoardComponent} from './board.component';
//noinspection TypeScriptValidateTypes
@Component({
    selector: 'my-app',
    templateUrl: 'app/templates/app.component.html',
    directives: [BoardComponent]
})
export class AppComponent { }