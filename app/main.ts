import {bootstrap} from '@angular/platform-browser-dynamic';

import { AppComponent } from './components/app.component';

//noinspection TypeScriptValidateTypes
bootstrap(AppComponent);